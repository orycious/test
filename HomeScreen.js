import React from 'react';
import {
    Image,
    View,
    TouchableOpacity,
    AppRegistry,
    StyleSheet,
    StatusBar,
    Dimensions,
    ScrollView
} from 'react-native';
import {
    Container,
    Button,
    Drawer,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Left,
    Right,
    Body,
    Icon,
    Text,
    Card,
    CardItem
} from 'native-base';
import { StackNavigator, TabNavigator } from 'react-navigation';
import Swiper from 'react-native-swiper';

import SetupScreen from './app/screens/SetupScreen';
import Pengaduanbox from './app/boxbtn/Pengaduanbox';
import Pantauan from './app/boxbtn/Pantauan';
import Galeribox from './app/boxbtn/Galeribox';
import About from './app/boxbtn/About';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const { width, height } = Dimensions.get('window')

class HomeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            visibleSwiper: false
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                visibleSwiper: true
            });
        }, 100);
    }

    render() {
        let swiper = null;
        if (this.state.visibleSwiper) {
            swiper = <Swiper 
                horizontal={true}
                loop={false} bounces={true}
                dot={<View style={{ display: 'none' }} />}
                activeDot={<View style={{ display: 'none' }} />}
                removeClippedSubviews={false}
                showsButtons
            >
                <View style={styles.slide}>
                    <Image resizeMode='cover' style={styles.image} source={require('./app/images/1.jpg')} />
                </View>
                <View style={styles.slide}>
                    <Image resizeMode='cover' style={styles.image} source={require('./app/images/2.jpg')} />
                </View>
                <View style={styles.slide}>
                    <Image resizeMode='cover' style={styles.image} source={require('./app/images/3.jpg')} />
                </View>
            </Swiper>;
        } else {
            swiper = <View></View>;
        }
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Header style={{ backgroundColor: '#87CEFA' }} androidStatusBarColor='#00B0FF'>
                    <Body style={{ paddingLeft: 120 }}>
                        <Title style={{ fontWeight: '700' }}>DLHK Servis</Title>
                    </Body>
                    <Right>
                        <Button transparent
                            onPress={() => navigate('Setup')}>
                            <Icon style={styles.textbox2} name="notifications" />
                        </Button>
                    </Right>
                </Header>
                <ScrollView>
                    <View style={styles.rowbox}>
                        <TouchableOpacity onPress={() => navigate('Pengaduanbox')}>
                            <View style={styles.box1}>
                                <Text style={styles.textbox}>Pengaduan</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('Pantauan')}>
                            <View style={styles.box2}>
                                <Text style={styles.textbox}>Pantauan</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.rowbox}>
                        <TouchableOpacity onPress={() => navigate('Galeri')}>
                            <View style={styles.box3}>
                                <Text style={styles.textbox}>Galeri</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('About')}>
                            <View style={styles.box4}>
                                <Text style={styles.textbox}>Tentang Kami</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.position}>
                        <Text style={styles.ttext}>Berita Terbaru</Text>
                    </View>
                    <View style={styles.container}>
                        <StatusBar barStyle='light-content' />
                        {swiper}
                    </View>
                    <View style={styles.position}>
                        <Text style={styles.ttext}>Hubungi Kami</Text>
                    </View>
                    <View style={{ marginHorizontal: 20, marginTop: 10 }}>
                        <View style={styles.viewbox}>
                            <Text style={styles.textbox2}>(031) 8963184</Text>
                            <Text style={styles.textbox2}>blh.sidoarjokab@gmail.com</Text>
                            <Text style={styles.textbox2}>Jl.Raya Siwalan Panji No. 36, Buduran,</Text>
                            <Text style={styles.textbox2}>Sidoarjo</Text>
                        </View>
                    </View>
                    <View style={{ marginBottom: 20 }} />
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    slide: {
        flex: 2,
        marginHorizontal: 10,
        backgroundColor: 'transparent'
    },
    container: {
        height: 150,
        marginTop: 10,
        marginHorizontal: 10
    },
    image: {
        width,
        height: 180
    },
    position: {
        marginLeft: 20,
        marginTop: 20
    },
    rowbox: {
        flexDirection: 'row',
        marginHorizontal: 20,
        marginTop: 20
    },
    textbox: {
        color: 'whitesmoke',
        fontSize: 18
    },
    textbox2: {
        color: 'whitesmoke'
    },
    ttext: {
        fontWeight: '500'
    },
    box1: {
        width: 150,
        height: 110,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00B0FF'
    },
    box2: {
        width: 150,
        height: 110,
        borderRadius: 5,
        marginLeft: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'turquoise'
    },
    box3: {
        width: 150,
        height: 110,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FF4081'
    },
    box4: {
        width: 150,
        height: 110,
        borderRadius: 5,
        marginLeft: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FBC02D'
    },
    viewbox: {
        height: 150,
        backgroundColor: '#40C4FF',
        borderRadius: 5,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export const App = StackNavigator({
    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
            header: null
        }
    },
    Setup: {
        screen: SetupScreen,
        navigationOptions: {
            title: 'Pemberitahuan'
        }
    },
    Pengaduanbox: {
        screen: Pengaduanbox,
        navigationOptions: {
            title: 'Pengaduan'
        }
    },
    Pantauan: {
        screen: Pantauan,
        navigationOptions: {
            title: 'Pantauan'
        }
    },
    Galeri: {
        screen: Galeribox,
        navigationOptions: {
            title: 'Galeri'
        }
    },
    About: {
        screen: About,
        navigationOptions: {
            title: 'Tentang Kami'
        }
    }
});

export default App;