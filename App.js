import React from 'react';
import { AppRegistry } from 'react-native';
import { StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation';
import { Icon } from 'react-native-elements';

import HomeScreen from './HomeScreen';
import Screen2 from './app/screens/Screen2';
import Screen3 from './app/screens/Screen3';

export const tabs = TabNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Home',
      tabBarIcon: ({ focused, tintColor }) => (
        focused ?
          <Icon name="home" type="simple-line-icon" size={24} iconStyle={{ paddingBottom: 0, paddingTop: 0 }} color={ tintColor } /> :
          <Icon name="home" type="simple-line-icon" size={24} iconStyle={{ paddingBottom: 0, paddingTop: 0 }} color={ tintColor } />
      ),

    }
  },
  Screen2: {
    screen: Screen2,
    navigationOptions: {
      title: 'Search',
      tabBarIcon: ({ focused, tintColor }) => (
        focused ?
          <Icon name="magnifier" type="simple-line-icon" size={24} iconStyle={{ paddingBottom: 0, paddingTop: 0 }} color={ tintColor } /> :
          <Icon name="magnifier" type="simple-line-icon" size={24} iconStyle={{ paddingBottom: 0, paddingTop: 0 }} color={ tintColor } />

      ),

    }
  },

  Screen3: {
    screen: Screen3,
    navigationOptions: {
      title: 'Notifications',
      tabBarIcon: ({ focused, tintColor }) => (
        focused ?
          <Icon name="bell" type="simple-line-icon" size={24} iconStyle={{ paddingBottom: 0, paddingTop: 0 }} color={ tintColor } /> :
          <Icon name="bell" type="simple-line-icon" size={24} iconStyle={{ paddingBottom: 0, paddingTop: 0 }} color={ tintColor } />

      ),

    }
  },

},
  {
    lazy: true,
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
    animationEnabled: true,
    swipeEnabled: false,
  }
);


export default tabs;